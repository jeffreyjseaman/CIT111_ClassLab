
public class Employee {
	private int empNum;
	private String emplLastname;
	private String empFirstname;
	private double empSalary;
	
	//Get Object
	public int getEmpNum()
	{
		return empNum;
	}
	
	public void setEmpNum(int emp)
	{
		empNum = emp;
	}
	
	//Get Employee Lastname
	public String getEmpLastname()
	{
		return emplLastname;
	}
	
	public void setEmplastname(String name)
	{
		emplLastname = name;
	}
	
	public String getEmpFirstname()
	{
		return empFirstname;
	}
	
	public void setEmpFirstname(String name)
	{
		empFirstname = name;
	}
	
	public double getEmpSalary()
	{
		return empSalary;
	}
	
	public void setEmpSalary(double sal)
	{
		empSalary = sal;
	}
	
	
}
